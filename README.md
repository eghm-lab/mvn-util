# mvn-util

## MvnLinks
groovy -Dmvn.repo.home=/j/m2 -Dmvn.repository.dir=r bin/MvnLinks.groovy kuali rice_2_4 org.kuali

![Image of mvnLinks](https://raw.githubusercontent.com/eghm/mvn-util/master/doc/img/mvnLinks-annotated.png)

